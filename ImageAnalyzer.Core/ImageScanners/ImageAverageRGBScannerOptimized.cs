﻿using ImageAnalyzer.Core.Dto;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageAnalyzer.Core.ImageScanners
{
    public class ImageAverageRGBScannerOptimized : IImageAverageRGBScanner
    {
        public IEnumerable<BoxData> Scan(int boxWidth, int boxHeight, Bitmap bitmap)
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);

            int bytesPerPixel = Image.GetPixelFormatSize(bitmap.PixelFormat) / 8;
            int heightInPixels = bitmapData.Height;
            int widthInPixels = bitmapData.Width;
            int widthInBytes = bitmapData.Width * bytesPerPixel;

            int verticalBoxesCount = heightInPixels + 1 - boxHeight;
            int hotizontalBoxesCount = widthInPixels + 1 - boxWidth;
            int boxesCount = (verticalBoxesCount > 0 ? verticalBoxesCount : 1) * (hotizontalBoxesCount > 0 ? hotizontalBoxesCount : 1);

            List<BoxData> boxesData = new(boxesCount);

            unsafe
            {
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                for (int boxPosY = 0; boxPosY <= heightInPixels - boxHeight; boxPosY++)
                {
                    for (int boxPosX = 0; boxPosX <= widthInPixels - boxWidth; boxPosX++)
                    {
                        int red = 0;
                        int green = 0;
                        int blue = 0;

                        for (int y = boxPosY; y < boxPosY + boxHeight; y++)
                        {
                            byte* currentLine = ptrFirstPixel + y * bitmapData.Stride;

                            for (int x = boxPosX * bytesPerPixel; x < (boxPosX + boxWidth) * bytesPerPixel; x += bytesPerPixel)
                            {
                                blue += currentLine[x];
                                green += currentLine[x + 1];
                                red += currentLine[x + 2];
                            }
                        }

                        int totalNumberOfPixels = boxWidth * boxHeight;
                        byte blueAvg = (byte)(blue / totalNumberOfPixels);
                        byte greenAvg = (byte)(green / totalNumberOfPixels);
                        byte redAvg = (byte)(red / totalNumberOfPixels);

                        boxesData.Add(new BoxData(new BoxCoordinates(boxPosX, boxPosY), new RGBValues(redAvg, greenAvg, blueAvg)));
                    }
                }
            }

            bitmap.UnlockBits(bitmapData);

            return boxesData;
        }
    }
}

