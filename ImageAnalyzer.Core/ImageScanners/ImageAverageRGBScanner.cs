﻿using ImageAnalyzer.Core.Dto;
using System.Collections.Generic;
using System.Drawing;

namespace ImageAnalyzer.Core.ImageScanners
{
    public class ImageAverageRGBScanner : IImageAverageRGBScanner
    {
        private readonly IBoxAverageRGBAnalyzer _boxAverageRGBCounter;

        public ImageAverageRGBScanner(IBoxAverageRGBAnalyzer boxAverageRGBCounter)
        {
            _boxAverageRGBCounter = boxAverageRGBCounter;
        }

        public IEnumerable<BoxData> Scan(int boxWidth, int boxHeight, Bitmap bitmap)
        {
            for (int boxPositionY = 0; boxPositionY <= bitmap.Height - boxHeight; boxPositionY++)
            {
                for (int boxPositionX = 0; boxPositionX <= bitmap.Width - boxWidth; boxPositionX++)
                {
                    yield return _boxAverageRGBCounter.GetBoxResult(boxWidth, boxHeight, boxPositionX, boxPositionY, bitmap);
                }
            }
        }
    }
}
