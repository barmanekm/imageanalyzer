﻿using ImageAnalyzer.Core.Dto;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ImageAnalyzer.Core.FixedSizeCollections
{
    public class FixedSizeSortedResultsSynchronizedDictionary : IResultsCollection
    {
        private readonly int _sizeLimit;
        private readonly SortedDictionary<Difference, Result> _inner;

        public int Count => _inner.Count;

        public void Add(Result item)
        {
            lock (this)
            {
                _inner.Add(new Difference(item.AbsoluteDifference), item);
                if (_inner.Count > _sizeLimit)
                {
                    _inner.Remove(_inner.First().Key);
                }
            }
        }


        public IEnumerator<Result> GetEnumerator()
        {
            return _inner.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _inner.Values.GetEnumerator();
        }

        public FixedSizeSortedResultsSynchronizedDictionary(int sizeLimit)
        {
            _sizeLimit = sizeLimit;
            _inner = new SortedDictionary<Difference, Result>(new IntComparerWithoutEquality());
        }

        private class IntComparerWithoutEquality : IComparer<Difference>
        {
            public int Compare(Difference x, Difference y)
            {
                int result = y.Value - x.Value;
                if (result != 0)
                    return result;

                if (x.Equals(y))
                    return 0;

                return -1;
            }
        }

        private class Difference
        {
            private readonly int value;

            public Difference(int value)
            {
                this.value = value;
            }

            public int Value => value;
        }
    }
}
