﻿using ImageAnalyzer.Core.Dto;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ImageAnalyzer.Core.FixedSizeCollections
{
    public class FixedSizeSortedResultsList : IResultsCollection
    {
        private readonly int _sizeLimit;
        private readonly SortedList<Difference, Result> _inner;

        public int Count => _inner.Count;

        public void Add(Result item)
        {
            if (_inner.Count < _sizeLimit)
            {
                _inner.Add(new Difference(item.AbsoluteDifference), item);
            }

            else if (item.AbsoluteDifference < _inner.Keys[_sizeLimit - 1].Value)
            {
                _inner.RemoveAt(_sizeLimit - 1);
                _inner.Add(new Difference(item.AbsoluteDifference), item);
            }     
        }

        public IEnumerator<Result> GetEnumerator()
        {
            return _inner.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _inner.Values.GetEnumerator();
        }

        public FixedSizeSortedResultsList(int sizeLimit)
        {
            _sizeLimit = sizeLimit;
            _inner = new SortedList<Difference, Result>(new IntComparerWithoutEquality());
        }

        private class IntComparerWithoutEquality : IComparer<Difference>
        {
            public int Compare(Difference x, Difference y)
            {
                int result = x.Value - y.Value;
                if (result != 0)
                    return result;

                if (x.Equals(y))
                    return 0;

                return -1;
            }
        }

        private class Difference
        {
            private readonly int value;

            public Difference(int value)
            {
                this.value = value;
            }

            public int Value => value;
        }
    }
}
