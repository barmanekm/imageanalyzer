﻿using ImageAnalyzer.Core.Dto;
using System.Collections.Generic;

namespace ImageAnalyzer.Core.ImageSetScanners
{
    public interface IImageSetScanner
    {
        IEnumerable<Result> Scan(int boxWidth, int boxHeight, RGBValues referenceRgbValues, string directory);
    }
}