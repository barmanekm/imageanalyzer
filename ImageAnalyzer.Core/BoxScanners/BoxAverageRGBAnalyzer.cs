﻿using ImageAnalyzer.Core.Dto;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageAnalyzer.Core.BoxScanners
{
    public class BoxAverageRGBAnalyzer : IBoxAverageRGBAnalyzer
    {
        public BoxData GetBoxResult(int boxWidth, int boxHeight, int boxPositionX, int boxPositionY, Bitmap bitmap)
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(boxPositionX, boxPositionY, boxWidth, boxHeight), ImageLockMode.ReadOnly, bitmap.PixelFormat);

            int bytesPerPixel = Image.GetPixelFormatSize(bitmap.PixelFormat) / 8;
            int heightInPixels = bitmapData.Height;
            int widthInBytes = bitmapData.Width * bytesPerPixel;

            int red = 0;
            int green = 0;
            int blue = 0;

            unsafe
            {
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLine = ptrFirstPixel + y * bitmapData.Stride;
                    for (int x = 0; x < widthInBytes; x += bytesPerPixel)
                    {
                        blue += currentLine[x];
                        green += currentLine[x + 1];
                        red += currentLine[x + 2];
                    }
                }
            }

            bitmap.UnlockBits(bitmapData);

            int totalNumberOfPixels = bitmapData.Height * bitmapData.Width;
            byte blueAvg = (byte)(blue / totalNumberOfPixels);
            byte greenAvg = (byte)(green / totalNumberOfPixels);
            byte redAvg = (byte)(red / totalNumberOfPixels);

            return new BoxData(new BoxCoordinates(boxPositionX, boxPositionY), new RGBValues(redAvg, greenAvg, blueAvg));
        }
    }
}

