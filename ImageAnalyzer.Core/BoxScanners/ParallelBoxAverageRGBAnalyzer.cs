﻿using ImageAnalyzer.Core.Dto;
using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Threading.Tasks;

namespace ImageAnalyzer.Core.BoxScanners
{
    // Useless, very unefficient because of synchronization and thread creation overhead. At least for standard boxes sizes
    public class ParallelBoxAverageRGBAnalyzer : IBoxAverageRGBAnalyzer
    {

        public BoxData GetBoxResult(int boxWidth, int boxHeight, int boxPositionX, int boxPositionY, Bitmap bitmap)
        {
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(boxPositionX, boxPositionY, boxWidth, boxHeight), ImageLockMode.ReadWrite, bitmap.PixelFormat);

            int bytesPerPixel = Image.GetPixelFormatSize(bitmap.PixelFormat) / 8;
            int heightInPixels = bitmapData.Height;
            int widthInBytes = bitmapData.Width * bytesPerPixel;

            int red = 0;
            int green = 0;
            int blue = 0;

            unsafe
            {
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                var partitioner = Partitioner.Create(0, heightInPixels);

                Parallel.ForEach(
                    partitioner,
                    () => Tuple.Create(0, 0, 0), // initial state
                    (range, loopState, subTotal) => // local iteration
                    {
                        for (int y = range.Item1; y < range.Item2; y++)
                        {
                            byte* currentLine = ptrFirstPixel + y * bitmapData.Stride;
                            for (int x = 0; x < widthInBytes; x += bytesPerPixel)
                            {
                                subTotal = Tuple.Create(subTotal.Item1 + currentLine[x], subTotal.Item2 + currentLine[x + 1], subTotal.Item3 + currentLine[x + 2]);
                            }
                        }
                        return subTotal;
                    },
                    (subTotal) => // final result of local iteration
                    {
                        Interlocked.Add(ref blue, subTotal.Item1);
                        Interlocked.Add(ref green, subTotal.Item2);
                        Interlocked.Add(ref red, subTotal.Item3);
                    });
            }

            bitmap.UnlockBits(bitmapData);

            int totalNumberOfPixels = bitmapData.Height * bitmapData.Width;
            byte blueAvg = (byte)(blue / totalNumberOfPixels);
            byte greenAvg = (byte)(green / totalNumberOfPixels);
            byte redAvg = (byte)(red / totalNumberOfPixels);

            return new BoxData(new BoxCoordinates(boxPositionX, boxPositionY), new RGBValues(redAvg, greenAvg, blueAvg));
        }
    }
}

