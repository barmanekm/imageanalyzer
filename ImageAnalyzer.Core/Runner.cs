﻿using ImageAnalyzer.Core.Dto;
using ImageAnalyzer.Core.ImageSetScanners;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageAnalyzer.Core
{
    public class Runner
    {
        private readonly IImageSetScanner imageSetScanner;
        private readonly IResultsCollection resultsCollection;

        public Runner(IImageSetScanner imageSetScanner, IResultsCollection resultsCollection)
        {
            this.imageSetScanner = imageSetScanner;
            this.resultsCollection = resultsCollection;
        }

        public IEnumerable<Result> Run(int width, int height, short r, short g, short b, string directory)
        {
            IEnumerable<Result> results = imageSetScanner.Scan(
                width,
                height,
                new RGBValues(r, g, b),
                directory);

            foreach (Result result in results)
            {
                resultsCollection.Add(result);
            }

            return resultsCollection;
        }

    }
}
