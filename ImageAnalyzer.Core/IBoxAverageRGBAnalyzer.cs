﻿using ImageAnalyzer.Core.Dto;
using System.Drawing;

namespace ImageAnalyzer.Core
{
    public interface IBoxAverageRGBAnalyzer
    {
        BoxData GetBoxResult(int boxWidth, int boxHeight, int boxPositionX, int boxPositionY, Bitmap bitmap);
    }
}