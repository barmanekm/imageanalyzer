﻿using ImageAnalyzer.Core.Dto;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageAnalyzer.Core
{
    public class ReferenceRunner
    {
        private readonly Runner _runner;
        private readonly IBoxAverageRGBAnalyzer _boxAverageRGBAnalyzer;

        public ReferenceRunner(Runner runner, IBoxAverageRGBAnalyzer boxAverageRGBAnalyzer)
        {
            _runner = runner;
            _boxAverageRGBAnalyzer = boxAverageRGBAnalyzer;
        }

        public IEnumerable<Result> Run(int width, int height, int referenceBoxPosX, int referenceBoxPosY, string referenceFile, string directory)
        {
            BoxData referenceBox;
            using (Bitmap referenceImage = new(referenceFile))
            {
                referenceBox = _boxAverageRGBAnalyzer.GetBoxResult(width, height, referenceBoxPosX, referenceBoxPosY, referenceImage);
            }

            return _runner.Run(width, height, referenceBox.RGBValues.R, referenceBox.RGBValues.G, referenceBox.RGBValues.B, directory);
        }
    }
}
