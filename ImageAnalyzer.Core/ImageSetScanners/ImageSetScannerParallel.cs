﻿using ImageAnalyzer.Core.Dto;
using ImageAnalyzer.Core.IO;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace ImageAnalyzer.Core.ImageSetScanners
{
    public class ImageSetScannerParallel : IImageSetScanner
    {
        private readonly IImageAverageRGBScanner _imageScanner;
        private readonly ImageFilesSearcher _imageFilesSearcher;
        private readonly ILogger<IImageSetScanner> _logger;

        public ImageSetScannerParallel(IImageAverageRGBScanner imageScanner, ImageFilesSearcher imageFilesSearcher, ILogger<IImageSetScanner> logger)
        {
            _imageScanner = imageScanner;
            _imageFilesSearcher = imageFilesSearcher;
            _logger = logger;
        }

        public IEnumerable<Result> Scan(int boxWidth, int boxHeight, RGBValues referenceRgbValues, string directory)
        {
            List<string> filenames = _imageFilesSearcher.GetFiles(directory);

            return filenames.AsParallel()
                .SelectMany(f =>
                {
                    _logger.LogInformation("Scanning: {0}", f);
                    return _imageScanner.Scan(boxWidth, boxHeight, new Bitmap(f))
                    .Select(b => new Result(f, b.Coordinates, GetAbsoluteDifference(referenceRgbValues, b.RGBValues)));
                });
        }

        private static int GetAbsoluteDifference(RGBValues referenceRgbValues, RGBValues rgbActualValues)
        {
            var rgbDifference = rgbActualValues.Subtract(referenceRgbValues);
            int absoluteDifference = Math.Abs(rgbDifference.R) + Math.Abs(rgbDifference.G) + Math.Abs(rgbDifference.B) + Math.Abs(rgbDifference.Alpha);
            return absoluteDifference;
        }
    }
}
