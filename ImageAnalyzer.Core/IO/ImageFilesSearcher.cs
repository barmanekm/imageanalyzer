﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ImageAnalyzer.Core.IO
{
    public class ImageFilesSearcher
    {
        private const string SUPPORTED_EXTENSIONS = "*.png|*.jpeg|*.jpg|*.bmp";

        public List<String> GetFiles(String inputPath)
        {
            List<string> searchPatterns = new(SUPPORTED_EXTENSIONS.Split("|"));

            List<string> filenames = new();

            foreach (string searchPattern in searchPatterns)
            {
                filenames.AddRange(Directory.GetFiles(inputPath, searchPattern));
            }

            return filenames;
        }
    }
}
