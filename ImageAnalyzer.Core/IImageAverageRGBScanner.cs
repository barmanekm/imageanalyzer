﻿using ImageAnalyzer.Core.Dto;
using System.Collections.Generic;
using System.Drawing;

namespace ImageAnalyzer.Core
{
    public interface IImageAverageRGBScanner
    {
        IEnumerable<BoxData> Scan(int boxWidth, int boxHeight, Bitmap bitmap);
    }
}