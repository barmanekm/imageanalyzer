﻿using ImageAnalyzer.Core.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageAnalyzer.Core
{
    public interface IResultsCollection : IEnumerable<Result>
    {
        void Add(Result result);
    }
}
