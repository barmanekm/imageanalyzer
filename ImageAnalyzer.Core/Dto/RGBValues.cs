﻿using System;

namespace ImageAnalyzer.Core.Dto
{
    public sealed class RGBValues
    {
        public readonly short R;
        public readonly short G;
        public readonly short B;
        public readonly short Alpha;

        public RGBValues(short r, short g, short b) : this(r, g, b, 0)
        {
            
        }

        public RGBValues(short r, short g, short b, short alpha)
        {
            R = r;
            G = g;
            B = b;
            Alpha = alpha;
        }

        public RGBValues Subtract(RGBValues other)
        {
            return new RGBValues((short)(R - other.R), (short)(G - other.G), (short)(B - other.B), (short)(Alpha - other.Alpha));
        }

        public override bool Equals(object obj)
        {
            return obj is RGBValues values &&
                   R == values.R &&
                   G == values.G &&
                   B == values.B &&
                   Alpha == values.Alpha;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(R, G, B, Alpha);
        }
    }
}
