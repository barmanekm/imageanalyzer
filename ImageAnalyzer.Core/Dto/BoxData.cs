﻿using System;
using System.Collections.Generic;

namespace ImageAnalyzer.Core.Dto
{
    public sealed class BoxData
    {
        private readonly BoxCoordinates coordinates;
        private readonly RGBValues rgbValues;

        public BoxData(BoxCoordinates coordinates, RGBValues rgbValues)
        {
            this.coordinates = coordinates;
            this.rgbValues = rgbValues;
        }

        public RGBValues RGBValues => rgbValues;

        public BoxCoordinates Coordinates => coordinates;

        public override bool Equals(object obj)
        {
            return obj is BoxData result &&
                   EqualityComparer<BoxCoordinates>.Default.Equals(coordinates, result.coordinates) &&
                   EqualityComparer<RGBValues>.Default.Equals(rgbValues, result.rgbValues);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(coordinates, rgbValues);
        }
    }
}
