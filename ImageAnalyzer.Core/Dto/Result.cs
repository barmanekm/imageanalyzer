﻿using System;
using System.Collections.Generic;

namespace ImageAnalyzer.Core.Dto
{
    public sealed class Result
    {
        public string Filename { get; }
        public BoxCoordinates BoxCoordinates { get; }
        public int AbsoluteDifference { get; }

        public Result(string filename, BoxCoordinates boxCoordinates, int absoluteDifference)
        {
            Filename = filename;
            BoxCoordinates = boxCoordinates;
            AbsoluteDifference = absoluteDifference;
        }

        public override bool Equals(object obj)
        {
            return obj is Result other &&
                   Filename == other.Filename &&
                   EqualityComparer<BoxCoordinates>.Default.Equals(BoxCoordinates, other.BoxCoordinates) &&
                   AbsoluteDifference == other.AbsoluteDifference;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Filename, BoxCoordinates, AbsoluteDifference);
        }

        public override string ToString()
        {
            return String.Format("Filename: {0} \t Box Coordinates: {1}  \t Aboslute Difference: {2}", Filename, BoxCoordinates, AbsoluteDifference); 
        }
    }
}
