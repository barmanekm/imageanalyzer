﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ImageAnalyzer.Core.Dto
{
    public sealed class BitmapIdentifier
    {
        private readonly Bitmap _bitmap;
        private readonly String _name;

        public BitmapIdentifier(Bitmap bitmap, string name)
        {
            _bitmap = bitmap;
            _name = name;
        }

        public Bitmap Bitmap => _bitmap;

        public string Name => _name;

        public override bool Equals(object obj)
        {
            return obj is BitmapIdentifier identifier &&
                   EqualityComparer<Bitmap>.Default.Equals(_bitmap, identifier._bitmap) &&
                   _name == identifier._name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_bitmap, _name);
        }
    }
}
