﻿using System;

namespace ImageAnalyzer.Core.Dto
{
    public sealed class BoxCoordinates
    {
        private readonly int _x;
        private readonly int _y;

        public BoxCoordinates(int x, int y)
        {
            _x = x;
            _y = y;
        }

        public int X => _x;
        public int Y => _y;

        public override bool Equals(object obj)
        {
            return obj is BoxCoordinates coordinates &&
                   _x == coordinates._x &&
                   _y == coordinates._y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_x, _y);
        }

        public override string ToString()
        {
            return String.Format("({0}, {1})", _x, _y);
        }
    }
}
