﻿using ImageAnalyzer.Core.BoxScanners;
using ImageAnalyzer.Core.Dto;
using ImageAnalyzer.Core.FixedSizeCollections;
using ImageAnalyzer.Core.ImageScanners;
using ImageAnalyzer.Core.ImageSetScanners;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace ImageAnalyzer.Core.Tests.Integration
{

    public class ExampleFlowerFindTests
    {
        private const string ReferenceFilename = @"IntegrationImages\0091.png";
        private const string ImagesDirectory = @"IntegrationImages";
        private readonly ITestOutputHelper _output;

        public ExampleFlowerFindTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void FindFlowerTest()
        {

            BoxData referenceBox;
            using (Bitmap referenceImage = new(ReferenceFilename))
            {
                BoxAverageRGBAnalyzer boxAverageRGBAnalyzer = new();
                referenceBox = boxAverageRGBAnalyzer.GetBoxResult(100, 100, 0, 0, referenceImage);
            }

            FixedSizeSortedResultsList top5Results = new(5);

            Stopwatch sw = new();
            sw.Start();

            ImageSetScanner sut = new(new ImageAverageRGBScanner(new BoxAverageRGBAnalyzer()), new IO.ImageFilesSearcher(), new NullLogger<IImageSetScanner>());
            IEnumerable<Result> results = sut.Scan(100, 100, referenceBox.RGBValues, ImagesDirectory);
            foreach (Result result in results)
            {
                top5Results.Add(result);
            }

            sw.Stop();

            _output.WriteLine(String.Format("Elapsed={0}", sw.Elapsed));

            Assert.Equal(top5Results.First(), new Result(ReferenceFilename, new BoxCoordinates(0, 0), 0));
            Assert.Equal(5, top5Results.Count);
        }

        [Fact]
        public void FindFlowerTestParallelTop()
        {

            BoxData referenceBox;
            using (Bitmap referenceImage = new(ReferenceFilename))
            {
                BoxAverageRGBAnalyzer boxAverageRGBAnalyzer = new();
                referenceBox = boxAverageRGBAnalyzer.GetBoxResult(100, 100, 0, 0, referenceImage);
            }

            FixedSizeSortedResultsSynchronizedDictionary top5Results = new(5);

            Stopwatch sw = new();
            sw.Start();

            ImageSetScannerParallel sut = new(new ImageAverageRGBScanner(new BoxAverageRGBAnalyzer()), new IO.ImageFilesSearcher(), new NullLogger<IImageSetScanner>());
            IEnumerable<Result> results = sut.Scan(100, 100, referenceBox.RGBValues, ImagesDirectory);
            foreach (Result result in results)
            {
                top5Results.Add(result);
            }

            sw.Stop();

            _output.WriteLine(String.Format("Elapsed={0}", sw.Elapsed));

            Assert.Equal(top5Results.Last(), new Result(ReferenceFilename, new BoxCoordinates(0, 0), 0));
            Assert.Equal(5, top5Results.Count);
        }

        [Fact]
        public void FindFlowerTestParallelTopOptimizedScanner()
        {

            BoxData referenceBox;
            using (Bitmap referenceImage = new(ReferenceFilename))
            {
                BoxAverageRGBAnalyzer boxAverageRGBAnalyzer = new();
                referenceBox = boxAverageRGBAnalyzer.GetBoxResult(100, 100, 0, 0, referenceImage);
            }

            FixedSizeSortedResultsSynchronizedDictionary top5Results = new(5);

            Stopwatch sw = new();
            sw.Start();

            ImageSetScannerParallel sut = new(new ImageAverageRGBScannerOptimized(), new IO.ImageFilesSearcher(), new NullLogger<IImageSetScanner>());
            IEnumerable<Result> results = sut.Scan(100, 100, referenceBox.RGBValues, ImagesDirectory);
            foreach (Result result in results)
            {
                top5Results.Add(result);
            }

            sw.Stop();

            _output.WriteLine(String.Format("Elapsed={0}", sw.Elapsed));

            Assert.Equal(top5Results.Last(), new Result(ReferenceFilename, new BoxCoordinates(0, 0), 0));
            Assert.Equal(5, top5Results.Count);
        }

        [Fact]
        public void FindFlowerTestDictionary()
        {

            BoxData referenceBox;
            using (Bitmap referenceImage = new(ReferenceFilename))
            {
                BoxAverageRGBAnalyzer boxAverageRGBAnalyzer = new();
                referenceBox = boxAverageRGBAnalyzer.GetBoxResult(100, 100, 0, 0, referenceImage);
            }

            FixedSizeSortedResultsDictionary top5Results = new(5);

            Stopwatch sw = new();
            sw.Start();

            ImageSetScanner sut = new(new ImageAverageRGBScanner(new BoxAverageRGBAnalyzer()), new IO.ImageFilesSearcher(), new NullLogger<IImageSetScanner>());
            IEnumerable<Result> results = sut.Scan(100, 100, referenceBox.RGBValues, ImagesDirectory);
            foreach (Result result in results)
            {
                top5Results.Add(result);
            }

            sw.Stop();

            _output.WriteLine(String.Format("Elapsed={0}", sw.Elapsed));

            Assert.Equal(top5Results.Last(), new Result(ReferenceFilename, new BoxCoordinates(0, 0), 0));
            Assert.Equal(5, top5Results.Count);
        }


        [Fact(Skip = "This implementation is to slow")]
        public void FindFlowerTestParallel()
        {

            BoxData referenceBox;
            using (Bitmap referenceImage = new(ReferenceFilename))
            {
                ParallelBoxAverageRGBAnalyzer boxAverageRGBAnalyzer = new();
                referenceBox = boxAverageRGBAnalyzer.GetBoxResult(100, 100, 0, 0, referenceImage);
            }

            FixedSizeSortedResultsList top5Results = new(5);

            Stopwatch sw = new();
            sw.Start();

            ImageSetScanner sut = new(new ImageAverageRGBScanner(new ParallelBoxAverageRGBAnalyzer()), new IO.ImageFilesSearcher(), new NullLogger<IImageSetScanner>());
            IEnumerable<Result> results = sut.Scan(100, 100, referenceBox.RGBValues, ImagesDirectory);
            foreach (Result result in results)
            {
                top5Results.Add(result);
            }

            sw.Stop();

            _output.WriteLine(String.Format("Elapsed={0}", sw.Elapsed));

            Assert.Equal(top5Results.First(), new Result(ReferenceFilename, new BoxCoordinates(0, 0), 0));
            Assert.Equal(5, top5Results.Count);
        }

    }
}
