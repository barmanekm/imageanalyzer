﻿using ImageAnalyzer.Core.BoxScanners;
using ImageAnalyzer.Core.Dto;
using System.Drawing;
using Xunit;

namespace ImageAnalyzer.Core.Tests.BoxScanners
{
    public class BoxAverageRGBAnalyzerTests
    {
        [Theory]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 255, 0, 0)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_G.png", 0, 255, 0)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_B.png", 0, 0, 255)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_Black.png", 0, 0, 0)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_White.png", 255, 255, 255)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_RGB_h.png", 255 / 3, 255 / 3, 255 / 3)]
        public void BoxEqualToImageTest(string imageResourceName, short exptectedRedAvg, short expectedGreenAvg, short expectedBlueAvg)
        {
            using var imageStream = GetType().Assembly.GetManifestResourceStream(imageResourceName);
            using var testBitmap = new Bitmap(imageStream);
            BoxAverageRGBAnalyzer sut = new();
            BoxData actualAverageRGB = sut.GetBoxResult(3, 3, 0, 0, testBitmap);

            Assert.Equal(new BoxData(new BoxCoordinates(0, 0), new RGBValues(exptectedRedAvg, expectedGreenAvg, expectedBlueAvg)), actualAverageRGB);
        }
    }
}
