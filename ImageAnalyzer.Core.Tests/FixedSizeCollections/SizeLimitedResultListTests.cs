﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using ImageAnalyzer.Core.FixedSizeCollections;
using ImageAnalyzer.Core.Dto;

namespace ImageAnalyzer.Core.Tests.FixedSizeCollections
{

    public class SizeLimitedResultListTests
    {
        private readonly List<Result> expectedResults;

        public SizeLimitedResultListTests()
        {
            var expectedResult0 = new Result("file1.png", new BoxCoordinates(0, 0), 0);
            var expectedResult1 = new Result("file2.png", new BoxCoordinates(0, 0), 1);
            var expectedResult2 = new Result("file3.png", new BoxCoordinates(0, 0), 2);
            var expectedResult3 = new Result("file4.png", new BoxCoordinates(0, 0), 3);
            expectedResults = new();
            expectedResults.Add(expectedResult0);
            expectedResults.Add(expectedResult1);
            expectedResults.Add(expectedResult2);
            expectedResults.Add(expectedResult3);
        }

        [Fact]
        public void FixedSizeSortedResultsSynchronizedListTest()
        {
            FixedSizeSortedResultsSynchronizedList sut = new(3);
            PopulateSut(sut);

            List<Result> sutList = sut.ToList();

            AssertOrderAndContent(sutList);
        }

        [Fact]
        public void FixedSizeSortedResultsListTest()
        {
            FixedSizeSortedResultsList sut = new(3);
            PopulateSut(sut);

            List<Result> sutList = sut.ToList();

            AssertOrderAndContent(sutList);
        }

        [Fact]
        public void FixedSizeSortedResultsDictionaryTest()
        {
            FixedSizeSortedResultsDictionary sut = new(3);
            PopulateSut(sut);

            List<Result> sutList = sut.Reverse().ToList(); // Dictionaries will order top results in revers because of performance optimization

            AssertOrderAndContent(sutList);
        }

        [Fact]
        public void FixedSizeSortedResultsSynchronizedDictionaryTest()
        {
            FixedSizeSortedResultsSynchronizedDictionary sut = new(3);
            PopulateSut(sut);

            List<Result> sutList = sut.Reverse().ToList(); // Dictionaries will order top results in revers because of performance optimization

            AssertOrderAndContent(sutList);
        }


        private void PopulateSut(IResultsCollection sut)
        {
            sut.Add(expectedResults[1]);
            sut.Add(expectedResults[2]);
            sut.Add(expectedResults[0]);
            sut.Add(expectedResults[3]);
        }

        private void AssertOrderAndContent(List<Result> sutList)
        {
            Assert.Equal(expectedResults[0], sutList[0]);
            Assert.Equal(expectedResults[1], sutList[1]);
            Assert.Equal(expectedResults[2], sutList[2]);
            Assert.DoesNotContain(expectedResults[3], sutList);
        }
    }
}
