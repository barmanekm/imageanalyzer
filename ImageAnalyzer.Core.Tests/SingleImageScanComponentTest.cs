﻿using ImageAnalyzer.Core.BoxScanners;
using ImageAnalyzer.Core.Dto;
using ImageAnalyzer.Core.FixedSizeCollections;
using ImageAnalyzer.Core.ImageScanners;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ImageAnalyzer.Core.Tests
{
    public class SingleImageScanComponentTest
    {
        [Fact]
        public void ShouldFindFourColorBoxesOnTestImage()
        {
            FixedSizeSortedResultsDictionary results = new(4);

            using (var imageStream = this.GetType().Assembly.GetManifestResourceStream("ImageAnalyzer.Core.Tests.TestImages.100x100_with_RGB_test_boxes.png"))
            using (var testBitmap = new Bitmap(imageStream))
            {
                var boxAverageCounter = new BoxAverageRGBAnalyzer();
                var referenceRGBValues = boxAverageCounter.GetBoxResult(9, 2, 0, 0, testBitmap).RGBValues;

                foreach (var boxData in new ImageAverageRGBScanner(new BoxAverageRGBAnalyzer()).Scan(9, 3, testBitmap))
                {
                    results.Add(new Result("ImageAnalyzer.Core.Tests.TestImages.100x100_with_RGB_test_boxes.png", boxData.Coordinates, GetAbsoluteDifference(referenceRGBValues, boxData.RGBValues)));
                }
            }
            Assert.Contains(results, x => x.BoxCoordinates.Equals(new BoxCoordinates(0, 0)));
            Assert.Contains(results, x => x.BoxCoordinates.Equals(new BoxCoordinates(91, 0)));
            Assert.Contains(results, x => x.BoxCoordinates.Equals(new BoxCoordinates(0, 97)));
            Assert.Contains(results, x => x.BoxCoordinates.Equals(new BoxCoordinates(91, 97)));
            Assert.Equal(4, results.Count);
        }

        private static int GetAbsoluteDifference(RGBValues referenceRgbValues, RGBValues rgbActualValues)
        {
            var rgbDifference = rgbActualValues.Subtract(referenceRgbValues);
            int absoluteDifference = Math.Abs(rgbDifference.R) + Math.Abs(rgbDifference.G) + Math.Abs(rgbDifference.B) + Math.Abs(rgbDifference.Alpha);
            return absoluteDifference;
        }
    }
}
