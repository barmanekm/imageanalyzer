using Xunit;
using System.Drawing;
using System.Collections.Generic;
using Moq;
using ImageAnalyzer.Core.BoxScanners;
using ImageAnalyzer.Core.ImageScanners;
using ImageAnalyzer.Core.Dto;

namespace ImageAnalyzer.Core.Tests.ImageScanners
{
    public class ImageAverageRGBScannerTests
    {

        [Theory]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 1, 1, 9)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 1, 3, 3)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 3, 1, 3)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 2, 2, 4)]
        public void NumberOfScannedBoxesTest(string imageResourceName, int boxHeight, int boxWidth, int expectedBoxCount)
        {
            using var imageStream = GetType().Assembly.GetManifestResourceStream(imageResourceName);
            using var testBitmap = new Bitmap(imageStream);
            var boxAverageRGBCounterMock = new Mock<BoxAverageRGBAnalyzer>();
            ImageAverageRGBScanner sut = new(boxAverageRGBCounterMock.Object);

            IList<BoxData> actualAverageRGB = new List<BoxData>(sut.Scan(boxWidth, boxHeight, testBitmap));

            Assert.Equal(expectedBoxCount, actualAverageRGB.Count);
        }
    }


}


