﻿using ImageAnalyzer.Core.Dto;
using ImageAnalyzer.Core.ImageScanners;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ImageAnalyzer.Core.Tests.ImageScanners
{
    public class ImageAverageRGBScannerOptimizedTests
    {
        [Fact]
        public void ShouldFindFourColorBoxesOnTestImage()
        {
            List<BoxData> results = new();
            using (var imageStream = this.GetType().Assembly.GetManifestResourceStream("ImageAnalyzer.Core.Tests.TestImages.100x100_with_RGB_test_boxes.png"))
            using (var testBitmap = new Bitmap(imageStream))
            {
                ImageAverageRGBScannerOptimized sut = new ImageAverageRGBScannerOptimized();
                foreach (var boxData in sut.Scan(9,2, testBitmap))
                {
                    results.Add(boxData);
                }
            }

            Assert.Contains(results, x => x.Coordinates.Equals(new BoxCoordinates(0, 0)) && x.RGBValues.Equals(new RGBValues(255 / 3, 255 / 3, 255 / 3)));
            Assert.Contains(results, x => x.Coordinates.Equals(new BoxCoordinates(91, 0)) && x.RGBValues.Equals(new RGBValues(255 / 3, 255 / 3, 255 / 3)));
            Assert.Contains(results, x => x.Coordinates.Equals(new BoxCoordinates(0, 97)) && x.RGBValues.Equals(new RGBValues(255 / 3, 255 / 3, 255 / 3)));
            Assert.Contains(results, x => x.Coordinates.Equals(new BoxCoordinates(91, 97)) && x.RGBValues.Equals(new RGBValues(255 / 3, 255 / 3, 255 / 3)));
            Assert.Equal(9108, results.Count);
        }

        [Theory]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 1, 1, 9)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 1, 3, 3)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 3, 1, 3)]
        [InlineData("ImageAnalyzer.Core.Tests.TestImages.3x3_R.png", 2, 2, 4)]
        public void NumberOfScannedBoxesTestForOptimized(string imageResourceName, int boxHeight, int boxWidth, int expectedBoxCount)
        {
            using var imageStream = this.GetType().Assembly.GetManifestResourceStream(imageResourceName);
            using var testBitmap = new Bitmap(imageStream);

            ImageAverageRGBScannerOptimized sut = new();

            IList<BoxData> actualAverageRGB = new List<BoxData>(sut.Scan(boxWidth, boxHeight, testBitmap));

            Assert.Equal(expectedBoxCount, actualAverageRGB.Count);
        }
    }
}
