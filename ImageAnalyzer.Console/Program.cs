﻿
using ImageAnalyzer.Core;
using ImageAnalyzer.Core.BoxScanners;
using ImageAnalyzer.Core.FixedSizeCollections;
using ImageAnalyzer.Core.ImageScanners;
using ImageAnalyzer.Core.ImageSetScanners;
using ImageAnalyzer.Core.IO;
using McMaster.Extensions.CommandLineUtils;
using System;
using System.Diagnostics;

using Microsoft.Extensions.DependencyInjection;
using ImageAnalyzer.Core.Dto;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace ImageAnalyzer.CLI
{
    class Program
    {

        static void Main(string[] args)
        {
            var app = new CommandLineApplication();
            app.HelpOption("--help", true);
            app.Command("scanreference", ScanReference);
            app.Command("scan", Scan);

            app.OnExecute(() =>
            {
                app.ShowHelp();
            });

            app.Execute(args);
        }

        static ServiceProvider GetContainer(int numberOfElements)
        {
            var loggerFactory = LoggerFactory.Create(builder =>
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("ImageAnalyzer.Core", LogLevel.Information)
                    .AddSimpleConsole((options) => { options.SingleLine = true; options.IncludeScopes = false; }));


            var services = new ServiceCollection();
            services
                .AddSingleton<IImageSetScanner, ImageSetScannerParallel>()
                .AddSingleton<ImageFilesSearcher>()
                .AddSingleton<IImageAverageRGBScanner, ImageAverageRGBScannerOptimized>()
                .AddSingleton<IBoxAverageRGBAnalyzer, BoxAverageRGBAnalyzer>()
                .AddSingleton<Runner>()
                .AddSingleton<ReferenceRunner>()
                .AddSingleton<ILogger<IImageSetScanner>>((provider) => loggerFactory.CreateLogger<IImageSetScanner>())
                .AddTransient<IResultsCollection>((provider) => ActivatorUtilities.CreateInstance<FixedSizeSortedResultsSynchronizedDictionary>(provider, numberOfElements));
            return services.BuildServiceProvider();
        }

        private static void Scan(CommandLineApplication app)
        {
            var directoryOption = app.Option<string>("-d|--directory <DIRECTORY>", "The directory containing PNG files to analyze", CommandOptionType.SingleValue).IsRequired();
            var boxWidthOption = app.Option<int>("-w|--box-width <BOXWIDTH>", "Scanning box width", CommandOptionType.SingleValue).IsRequired();
            var boxHeightOption = app.Option<int>("-h|--box-height <BOXHEIGHT>", "Scanning box height", CommandOptionType.SingleValue).IsRequired();
            var redOption = app.Option<short>("-r|--red <RED>", "Reference average RED value", CommandOptionType.SingleValue).IsRequired();
            var greenOption = app.Option<short>("-g|--green <GREEN>", "Reference average GREEN value", CommandOptionType.SingleValue).IsRequired();
            var blueOption = app.Option<short>("-b|--blue <BLUE>", "Reference average BLUE value", CommandOptionType.SingleValue).IsRequired();
            var numberOfResultsOption = app.Option<int>("-n|--number-of-results <N>", "Number of N top results to display", CommandOptionType.SingleValue).IsRequired();

            app.OnExecute(() =>
            {
                Console.WriteLine("Scan");

                int width = boxWidthOption.ParsedValue;
                int height = boxHeightOption.ParsedValue;
                short r = redOption.ParsedValue;
                short g = greenOption.ParsedValue;
                short b = blueOption.ParsedValue;
                int n = numberOfResultsOption.ParsedValue;
                string directory = directoryOption.ParsedValue;

                using var container = GetContainer(n);

                Runner runner = container.GetRequiredService<Runner>();

                Stopwatch sw = new();
                sw.Start();

                IEnumerable<Result> results = runner.Run(width, height, r, g, b, directory);

                sw.Stop();

                Console.Out.WriteLine(String.Format("Elapsed time: {0}", sw.Elapsed));

                foreach (var result in results)
                    Console.WriteLine(result);
            });
        }

        private static void ScanReference(CommandLineApplication app)
        {
            var directoryOption = app.Option<string>("-d|--directory <DIRECTORY>", "The directory containing PNG files to analyze", CommandOptionType.SingleValue).IsRequired();
            var boxWidthOption = app.Option<int>("-w|--box-width <BOXWIDTH>", "Scanning box width", CommandOptionType.SingleValue).IsRequired();
            var boxHeightOption = app.Option<int>("-h|--box-height <BOXHEIGHT>", "Scanning box height", CommandOptionType.SingleValue).IsRequired();
            var numberOfResultsOption = app.Option<int>("-n|--number-of-results <N>", "Number of N top results to display", CommandOptionType.SingleValue).IsRequired();
            var referenceFileOption = app.Option<string>("-f|--reference-file <FILENAME>", "Reference PNG file", CommandOptionType.SingleValue);
            var referenceBoxPosXOption = app.Option<int>("-x|--reference-box-positon-x <X>", "Reference box position X", CommandOptionType.SingleValue);
            var referenceBoxPosYOption = app.Option<int>("-y|--reference-box-positon-y <Y>", "Reference box position Y", CommandOptionType.SingleValue);

            app.OnExecute(() =>
            {

                int width = boxWidthOption.ParsedValue;
                int height = boxHeightOption.ParsedValue;
                int n = numberOfResultsOption.ParsedValue;
                string directory = directoryOption.ParsedValue;
                string referenceFile = referenceFileOption.ParsedValue;
                int referenceBoxPosX = referenceBoxPosXOption.ParsedValue;
                int referenceBoxPosY = referenceBoxPosYOption.ParsedValue;

                using var container = GetContainer(n);

                ReferenceRunner runner = container.GetRequiredService<ReferenceRunner>();

                Stopwatch sw = new();
                sw.Start();

                IEnumerable<Result> results = runner.Run(width, height, referenceBoxPosX, referenceBoxPosY, referenceFile, directory);

                sw.Stop();

                Console.Out.WriteLine(String.Format("Elapsed time: {0}", sw.Elapsed));

                foreach (var result in results)
                    Console.WriteLine(result);

            });
        }
    }
}
